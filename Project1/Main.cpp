#include "Header.h"
#include <iostream>
#include <ctime>
#define SIZE_MASSIF 14

using namespace std;

void main()
{
	srand(time(NULL));
	int massif[SIZE_MASSIF][SIZE_MASSIF];
	int size = 0, cursor = 0;

	do {
		cout << "Enter 0 to get help or operation number: ";
		cin >> cursor;
		cout << "----------------------------" << endl;

		switch (cursor)
		{
		case 0:  Help();
			break;
		case 1: cout << "Input size: ";
			cin >> size;
			break;
		case 2: MassifFillingManually(massif, size);
			break;
		case 3: MassifFilling(massif, size);
			break;
		case 4: Print(massif, size);
			break;
		case 5: SearchMultiplication(massif, size);
			break;
		case 6: CheckSimilarityOfColumns(massif, size);
			break;
		case 7: SearchMaxElementAndSumOfElements(massif, size);
			break;
		case 8: cursor = 8;
			break;
		}

		cout << "----------------------------" << endl;
		cout << " " << endl;
	} while (cursor != 8);
}