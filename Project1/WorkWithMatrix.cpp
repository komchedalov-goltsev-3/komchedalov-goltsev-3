#include <iostream>

#define SIZE_MASSIF 14

using namespace std;

void Print(int item[SIZE_MASSIF][SIZE_MASSIF], int size);

void Help()
{
    cout << "1. Specify array size" << endl;
    cout << "2. Fill the array manually" << endl;
    cout << "3. Fill the array with random values" << endl;
    cout << "4. Array output" << endl;
    cout << "5. Search multiplication" << endl;
    cout << "6. CheckSimilarityOfColumns" << endl;
    cout << "7. Search max element and sum of elements" << endl;
    cout << "8. Exit " << endl;
}

void MassifFilling(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            item[i][j] = rand() % 13 - 6;
        }
    }

    cout << "The array is successfully filled!" << endl;
}

void MassifFillingManually(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    cout << "Fill the array" << endl;

    int element;
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cin >> element;
            item[i][j] = element;
        }
    }

    cout << "The array is successfully filled!" << endl;
}

void SearchMultiplication(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    int min;
    int massifOfLows[SIZE_MASSIF] = {};
    for (int j = 0; j < size; j++) {
        min = abs(item[1][j]);
        for (int i = 0; i < size; i++) {
            if (abs(item[i][j]) < min)
            {
                massifOfLows[j] = i;
                min = abs(item[i][j]);
            }
        }
    }

    int massifOfMultiplications[SIZE_MASSIF] = {};
    for (int j = 0; j < size; j++) {
        for (int i = massifOfLows[j] + 1; i < size; i++) {
            massifOfMultiplications[j] *= item[i][j];
        }

    }

    for (int j = 0; j < size; j++) {
        cout << j << ". " << massifOfMultiplications[j] << " " << endl;
    }
}

void CheckSimilarityOfColumns(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    bool check = false;
    for (int i = 0; i < size; i++) {
        for (int j = 1; j < size; j++) {
            if (item[i][1] == item[j][size])
            {
                check = true;
            }
        }
    }

    if (check)
    {
        cout << "The first and last column have the same elements" << endl;
    }
    else
    {
        cout << "The first and last column do not have the same elements" << endl;
    }
}

void SearchMaxElementAndSumOfElements(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    int sizeCopy, max = item[1][1], sum = 0, line, column;
    sizeCopy = trunc((size / 3.0) * 2);
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            sum = sum + item[i][j];
            if (item[i][j] > max)
            {
                max = item[i][j];
                line = i + 1;
                column = j + 1;
            }
        }
    }

    cout << "Indices of maximum element: " << line << ", " << column << endl;
    cout << "Max element: " << max << endl;
    cout << "Sought sum: " << sum << endl;
}

void Print(int item[SIZE_MASSIF][SIZE_MASSIF], int size)
{
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            cout.width(4);
            cout << item[i][j] << " ";
        }
        cout << endl;
    }
}