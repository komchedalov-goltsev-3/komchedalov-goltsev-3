#define SIZE_MASSIF 14

int cursor;
void Help();
void MassifFilling(int massif[SIZE_MASSIF][SIZE_MASSIF], int size);
void MassifFillingManually(int item[SIZE_MASSIF][SIZE_MASSIF], int size);
void SearchMultiplication(int item[SIZE_MASSIF][SIZE_MASSIF], int size);
void CheckSimilarityOfColumns(int item[SIZE_MASSIF][SIZE_MASSIF], int size);
void SearchMaxElementAndSumOfElements(int item[SIZE_MASSIF][SIZE_MASSIF], int size);
void Print(int item[SIZE_MASSIF][SIZE_MASSIF], int size);


